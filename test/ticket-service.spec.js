
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

chai.use(chaiAsPromised);

const sinon = require('sinon');

const {expect} = chai;
const appRoot = process.env.PWD;

const noop = () => {};

const makeTicketService = require(appRoot + '/src/ticket-service');

const isFunction = obj => (typeof obj === 'function');

describe('test the ticketService', function(){

    const dummyResult = { rows: [] }

    const mockCbInterface = {
        insert: sinon.stub(),
        upsert: sinon.stub(),
        get: sinon.stub(),
        getAllWithId: sinon.stub()
    };

    mockCbInterface.insert.returns(Promise.resolve(dummyResult));
    mockCbInterface.upsert.returns(Promise.resolve(dummyResult));
    mockCbInterface.get.returns(Promise.resolve(dummyResult));
    mockCbInterface.getAllWithId.returns(Promise.resolve(dummyResult));

    it('should export a function', function(){
        expect(makeTicketService).to.be.a('function');
    });

    it('should return an object', function(){
        const result = makeTicketService(mockCbInterface);
        expect(result).to.be.an('object');
    });

    describe('test the returned object', function(){

        const mockRow = [1,1,0];
        const mockRows = [mockRow];

        const expectedProperties = [
            'amendTicket',
            'createTicket',
            'getAllTickets',
            'getSingleTicket',
            'getTicketStatus',
            'lockTicket'
        ];

        it('should have the expected properties', function(){
            const ticketService = makeTicketService(mockCbInterface);
            expect(ticketService).to.have.all.keys(expectedProperties);
        });

        it('should return properties that are all functions', function(){
            const ticketService = makeTicketService(mockCbInterface);
            const result = expectedProperties.every(propName => isFunction(ticketService[propName]));
            expect(result).to.be.true;
        });



        describe('test the amendTicket function', function(){
            const {amendTicket} = makeTicketService(mockCbInterface);

            beforeEach(function(){
                mockCbInterface.get.resetHistory();
            });


            it('should return a promise', function(){
                const result = amendTicket('mockId', mockRows).catch(noop);
                expect(result).to.be.a('promise');
            });

            it('should call cbInterface.get', function(){
                amendTicket('mockId', mockRows).catch(noop);
                sinon.assert.calledOnce(mockCbInterface.get);
            });

            it('should call cbInterface.get with the expected parameters', function(){
                amendTicket('mockId', mockRows).catch(noop);
                sinon.assert.calledWithExactly(mockCbInterface.get, 'mockId');
            });



        });

        describe('test the createTicket function', function(){
            const {createTicket} = makeTicketService(mockCbInterface);

            it('should return a promise', function(){
                const result = createTicket(mockRows).catch(noop);
                expect(result).to.be.a('promise');
            });


        });

        describe('test the getAllTickets function', function(){
            const {getAllTickets} = makeTicketService(mockCbInterface);


            it('should return a promise', function(){
                const result = getAllTickets().catch(noop);
                expect(result).to.be.a('promise');
            });

        });

        describe('test the getSingleTicket function', function(){
            const {getSingleTicket} = makeTicketService(mockCbInterface);

            it('should return a promise', function(){
                const result = getSingleTicket('mockId').catch(noop);
                expect(result).to.be.a('promise');
            });

        });

        describe('test the getTicketStatus function', function(){
            const {getTicketStatus} = makeTicketService(mockCbInterface);

            it('should return a promise', function(){
                const result = getTicketStatus('ticketId').catch(noop);
                expect(result).to.be.a('promise');
            });

        });

        describe('test the lockTicket function', function(){
            const {lockTicket} = makeTicketService(mockCbInterface);

            beforeEach(function(){
                mockCbInterface.get.resetHistory();
                mockCbInterface.upsert.resetHistory();
            });

            it('should return a promise', function(){
                const result = lockTicket('mockId').catch(noop);
                expect(result).to.be.a('promise');
            });

            it('should call cbInterface.get()', function(){
                return lockTicket('mockId', mockRows).then(() => {
                    return sinon.assert.calledOnce(mockCbInterface.get);    
                });
                
            });

            it('should call cbInterface.get() with the expected parameters', function(){
                lockTicket('mockId', mockRows).catch(noop);
                sinon.assert.calledWithExactly(mockCbInterface.get, 'mockId');
            });

            it('should call cbInterface.upsert()', function(){

                const mockRow = [1,1,0];
                const mockRows = [mockRow];

                mockCbInterface.get.returns(Promise.resolve({
                    locked: false,
                    rows: mockRows
                }));

                return lockTicket('mockId', mockRows).then(() => {
                    return sinon.assert.calledOnce(mockCbInterface.upsert);
                });
            });

            it('should call cbInterface.upsert() with the locked propety set to true', function(){

                const mockRow = [1,1,0];
                const mockRows = [mockRow];

                mockCbInterface.get.returns(Promise.resolve({
                    locked: false,
                    rows: mockRows
                }));

                return lockTicket('mockId', mockRows).then(() => {
                    const [,body] = mockCbInterface.upsert.getCall(0).args;
                    return expect(body.locked).to.be.true;

                });
            });
        });
    });
});