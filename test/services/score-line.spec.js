
const chai = require('chai');
const {expect} = chai;
const appRoot = process.env.PWD;

const scoreLine = require(appRoot + '/src/services/score-line');


describe('test the score-tickets module', function(){

    it('should export a function', function(){
        expect(scoreLine).to.be.a('function');
    });

    describe('if the sum of the three numbers is 2', function(){

        it('should return a score of 10, case 1', function(){
            const result = scoreLine([0, 0, 2]);
            expect(result).to.equal(10);

        });

        it('should return a score of 10, case 2', function(){
            const result = scoreLine([1, 0, 1]);
            expect(result).to.equal(10);

        });

        it('should return a score of ten, case 3', function(){
            const result = scoreLine([1, 1, 0]);
            expect(result).to.equal(10);
        });

    });

    describe('if all the numbers are the same', () => {

        it('should return a score of five, case 1', () => {
            const result = scoreLine([0,0,0]);
            expect(result).to.equal(5);
        });

        it('should return a score of five, case 2', () => {
            const result = scoreLine([1,1,1]);
            expect(result).to.equal(5);
        });

        it('should return a score of five, case 3', () => {
            const result = scoreLine([2,2,2]);
            expect(result).to.equal(5);
        });

    });

    describe('if the numbers after the first are different to the first number', () => {
        it('should return one as the score, case 1', function(){
            const result = scoreLine([0,1,2]);
            expect(result).to.equal(1);
        });

        it('should return one as the score, case 2', function(){
            const result = scoreLine([0,2,1]);
            expect(result).to.equal(1);
        });

        it('should return one as the score, case 3', function(){
            const result = scoreLine([0,2,2]);
            expect(result).to.equal(1);
        });

        it('should return one as the score, case 4', function(){
            const result = scoreLine([0,1,2]);
            expect(result).to.equal(1);
        });

    })
});
