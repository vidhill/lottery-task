/**
 * Created by davidhill on 11/11/2018.
 */

const chai = require('chai');
const {expect} = chai;
const appRoot = process.env.PWD;

const linesValidator = require(appRoot + '/src/services/validate-lines');


describe('test the validate-line module', function() {

    it('should export an object', function () {
        expect(linesValidator).to.be.a('object');
    });

    it('should have the expected properties', function(){
        const expectedProperties = ['validateLine', 'validateLines'];
        expect(linesValidator).to.have.all.keys(expectedProperties);
    });

    describe('test the validateLine function', function() {

        const {validateLine} = linesValidator;

        it('should return an object as a result', function () {
            const result = validateLine([1, 0]);
            expect(result).to.be.an('object');
        });

        describe('test the returned object', function () {

            it('should have the expected properties, valid case', function () {
                const expectedProperties = ['message', 'valid'];
                const result = validateLine([1, 0, 0]);
                expect(result).to.have.all.keys(expectedProperties);
            });

            it('should have the expected properties, invalid case', function () {
                const expectedProperties = ['message', 'valid'];
                const result = validateLine([1, 0]);
                expect(result).to.have.all.keys(expectedProperties);
            });

        });

        describe('if the parameter is not an Array', function () {

            it('should return false on the valid property', function () {
                const {valid} = validateLine(0);
                expect(valid).to.be.false;
            });

        });

        describe('if the array length is not the expected length', function () {

            it('should return false on the valid property', function () {
                const {valid} = validateLine([1, 0]);
                expect(valid).to.be.false;
            });

        });

        describe('if the array length is of the expected length', function () {

            it('should return true on the valid property', function () {
                const {valid} = validateLine([1, 0, 0]);
                expect(valid).to.be.true;
            });
        });

        describe('if the numbers are out of bounds', function () {

            it('should return false on the valid property', function () {
                const {valid} = validateLine([1, 0, 3]);
                expect(valid).to.be.false;
            });

        });

    });


    describe('test the linesValidator function', function(){
        const {validateLines} = linesValidator;

        it('should be a function', function(){
            expect(validateLines).to.be.a('function')
        });
        
        it('should ', function(){
            const testArray = [[0,0], [0,0,0]];
            const result = validateLines(testArray);
            expect(result.length).to.equal(1);
        });

        it('should ', function(){
            const testArray = [[0,0,1], [0,0,0]];
            const result = validateLines(testArray);
            expect(result.length).to.equal(0);
        });        
        

    });




});