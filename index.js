const express = require('express');
const app = express();
const port = process.env.PORT || 8080;
const logger = require('./src/logger-util');
const setupDb = require('./src/services/database-setup');

const initializeRoutes = require('./src/routes');

const cbInterface = setupDb();

const routes = initializeRoutes(cbInterface);

app.use(routes);

app.listen(port, () => logger.success(`App listening on port ${port}`));

