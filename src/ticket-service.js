/**
 * Created by davidhill on 10/11/2018.
 */

const cuid = require('cuid');
const logger = require('./logger-util');

const scoreLine = require('./services/score-line');

const createNewTicketObj = rows => ({
    rows,
    locked: false
});

const sortByScore = (a, b)=> {
    if(a.score === b.score) {
        return 0;
    } else {
        return b.score - a.score;
    }
};



function setup(cbInterface) {

    const createTicket = (reqBody) => {
        const ticketId = cuid();

        const ticket = createNewTicketObj(reqBody);

        return cbInterface.insert(ticketId, ticket).then(() => {

            const message = {
                ticketId
            };

            return Object.assign({}, message, ticket);
        });

    };

    const getAllTickets = () => {
        return cbInterface.getAllWithId()
            .then(documents => {
                const tickets = documents.map(result => ({
                    ticketId: result.id,
                    ticket: result.document
                }));

                return tickets;
            });
    };

    const getSingleTicket = ticketId => {
        return cbInterface.get(ticketId)
            .catch(err => {
                logger.debug(err);
                return Promise.resolve();
            })
    };

    const getTicketStatus = ticketId => {

        const processResult = result => {
            const ticketResult = result.rows.map((row, index) => {
                const score = scoreLine(row);
                return {
                    score,
                    row,
                    lineNumber: index + 1
                };
            })
            .sort(sortByScore);

            return ticketResult;
        };

        return cbInterface.get(ticketId)
                    .then(processResult)
                    .then(res => {
                        return lockTicket(ticketId)
                            .then(() => res);

                    })
            .catch(err => {
                console.log(err);
                Promise.resolve()
            });
    };

    const amendTicket = async function(ticketId, additionalRows) {
        const ticket = await cbInterface.get(ticketId);
        const {rows, locked} = ticket;

        if(locked === true){
            return Promise.resolve(false);
        }

        const newRows = [...rows, ...additionalRows];

        const newDocument = Object.assign({}, ticket, { rows: newRows})

        return cbInterface.upsert(ticketId, newDocument)
                    .then(() => newDocument);

    };


    const lockTicket = async function(ticketId) {
        const ticket = await cbInterface.get(ticketId);
        const newDocument = Object.assign({}, ticket, { locked: true});
        return cbInterface.upsert(ticketId, newDocument)
                    .then(() => newDocument);
    };


    return {
        createTicket,
        getAllTickets,
        getSingleTicket,
        getTicketStatus,
        amendTicket,
        lockTicket
    }
}

module.exports = setup;