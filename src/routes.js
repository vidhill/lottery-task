
const express = require('express');
const makeController = require('./ticket-controller');

const healthHandler = (req, res) => {
    const status = {
        status: 'ok'
    };
    res.send(status)
};


function setup(cbInterface){

    const app = express();

    app.use(express.json());

    const controller = makeController(cbInterface);

    app.get('/health', healthHandler);
    app.post('/ticket', controller.createTicket);
    app.get('/ticket', controller.getAllTickets);
    app.get('/ticket/:ticketId', controller.getSingleTicket);
    app.put('/ticket/:ticketId', controller.amendTicketLines);
    app.put('/status/:ticketId', controller.getTicketStatus);

    return app;
}


module.exports = setup;