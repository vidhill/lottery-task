/**
 * Created by davidhill on 11/11/2018.
 */

const createAllMatchPredicate = function(numberToMatch) {
    return num => (num === numberToMatch);
};

const createNotMatchPredicate = function(numberToMatch) {
    return num => (num !== numberToMatch);
};


const createSumOfNumbers = function(arr){
    return arr.reduce((aggregate, currentValue) => currentValue + aggregate, 0);
};

const checkAllMatch = function (arr) {
    return arr.every(createAllMatchPredicate(arr[0]));
};

const kdkdk = function(numbersArray){

    const [firstItem, ...remainingItems] = [...numbersArray];

    return remainingItems.every(createNotMatchPredicate(firstItem));
};


function scoreLine(numbersArr){
    /*
    if(numbersArr.length == 0){
        return
    }
    */

    if(createSumOfNumbers(numbersArr) === 2) {
        return 10;
    } else if(checkAllMatch(numbersArr)){
        return 5;
    } else if(kdkdk(numbersArr)){
        return 1;
    }
    return 0;
}


module.exports = scoreLine;