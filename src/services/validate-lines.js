/**
 * Created by davidhill on 11/11/2018.
 */

const incorrectLengthMessage = 'Incorrect number of numbers in the lottery line';
const incorrectRangeMessage = 'Numbers in this lottery line are outside the expected range';

const createErrorResult = message => ({ valid: false, message });
const config = require('../../config');

const {minNumber, maxNumber, count} = config.get('numberRange');

function validateLine(numbers) {

    if(!Array.isArray(numbers)){
        return createErrorResult('Line is not a valid array');
    }

    const expectedArrayLength = (numbers.length === count);

    const withinRange = numbers.every(number => (number >= minNumber) && (number <= maxNumber));

    if(!expectedArrayLength){
        return createErrorResult(incorrectLengthMessage);
    } else if(!withinRange){
        return createErrorResult(incorrectRangeMessage);
    }
    return {
        valid: true,
        message: 'ok'
    };
}

// returns array of invalid lines
function validateLines(lines) {

    const result = lines.map(validateLine);
    const isAllLinesValid = result.every(result => result.valid === true);

    if(isAllLinesValid === true) {
        return []; // no invalid lines
    } else {

        const invalidLines = result
            .map((result, index) => {
                return Object.assign({}, result, {
                    lineNumber: index + 1 // identify line number
                });

            })
            .filter(result => result.valid !== true);

        return invalidLines;

    }

}


module.exports = {
    validateLine,
    validateLines
};
