/**
 * Created by davidhill on 11/11/2018.
 */

const couchbase = require('couchbase');
const config = require('../../config');
const logger = require('../logger-util');



const {cbHost, bucketId} = config.get('db');

const prepareQuery = couchbase.N1qlQuery.fromString;


const createGenericHandler = (resolve, reject, property) => {
    return (err, result) => {
        if(err){
            reject(err);
        } else {
            return (property === undefined) ? resolve(result) : resolve(result[property]);
        }
    }
};


function setup(){
    const connectionString = 'couchbase://' + cbHost;
    const cluster = new couchbase.Cluster(connectionString);
    const bucket = cluster.openBucket(bucketId, function (err) {
        if(err){
            logger.fatal(err);
        } else {
            logger.success(`Successfully connected to couchbase bucket: '${bucketId}', using connectionString '${connectionString}'`);
        }
    });

    const queryAll = `SELECT * FROM ${bucketId}`;
    const queryAllWithId = `SELECT *, meta().id FROM ${bucketId}`;

    const getAll = function () {
        return new Promise((resolve, reject) => {
            const query = prepareQuery(queryAll);
            bucket.query(query, function (err, rows) {
                if(err){
                    reject(err);
                } else {
                    const results = rows.map(row => row[bucketId]);
                    resolve(results);
                }
            })
        }).catch(err => {
            logger.debug(err);
        })
    };

    const getAllWithId = function () {
        return new Promise((resolve, reject) => {
            const query = prepareQuery(queryAllWithId);
            bucket.query(query, function (err, rows) {
                if(err){
                    reject(err);
                } else {
                    const results = rows.map(row => {
                        const {id} = row;
                        return {
                            document: row[bucketId],
                            id
                        };
                    });
                    resolve(results);
                }
            })
        }).catch(err => {
            logger.debug(err);
        })
    };

    const getSingleDoc = function (documentId) {
            return new Promise((resolve, reject) => {
                bucket.get(documentId, createGenericHandler(resolve, reject, 'value'))
            })
    };


    const insertDoc = function(id, document){
        return new Promise((resolve, reject) => {
            bucket.insert(id, document, createGenericHandler(resolve, reject))
        })
    };

    const upsertDoc = function (id, document) {
        return new Promise((resolve, reject) => {
            bucket.upsert(id, document, createGenericHandler(resolve, reject))
        })
    };


    return {
        insert: insertDoc,
        upsert: upsertDoc,
        get: getSingleDoc,
        getAll,
        getAllWithId
    };

}


module.exports = setup;