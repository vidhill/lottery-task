/**
 * Created by davidhill on 10/11/2018.
 */

const makeTicketService = require('./ticket-service');
const {validateLines} = require('./services/validate-lines');
const logger = require('./logger-util');

const genericExceptionHandler = res => {
    return err => {
        logger.fatal(err);
        res.status(500)
            .send();

    }
};

const noTicketMessage = 'No ticket with that id exists';

function setup(cbInterface){

    const ticketService = makeTicketService(cbInterface);

    const createTicket = (req, res) => {

        const invalidLines = validateLines(req.body);

        if(invalidLines.length > 0){
            res.status(400)
                .send({
                    message: 'One or more lines have invalid numbers',
                    invalidLines
                });
            return;
        }

        ticketService.createTicket(req.body)
            .then(ticket => {
                res.send({
                    message: "Ticket successfully created",
                    ticket
                });
                return;
            })
            .catch(genericExceptionHandler(res))
    };
    

    const getAllTickets = (req, res) => {
        ticketService.getAllTickets()
            .then(tickets => {
                    res.send(tickets);
                    return;
            })

    };

    const getSingleTicket = (req, res) => {
        const {ticketId} = req.params;
        
        ticketService.getSingleTicket(ticketId)
            .then(ticket => {

                if(ticket === undefined){
                    res
                        .status(404)
                        .send({
                            message: noTicketMessage
                        });
                    return;
                } else {
                    res.send(ticket);
                    return;
                }

            });
    };

    const getTicketStatus = (req, res) => {
        const {ticketId} = req.params;

        ticketService.getTicketStatus(ticketId)
            .then(ticketStatus => {

                if(ticketStatus === undefined){
                    res
                        .status(404)
                        .send({
                            message: noTicketMessage
                        });
                    return;
                } else {
                    res.send(ticketStatus);
                    return;
                }

            });
    };


    const amendTicketLines = (req, res) => {
        const {ticketId} = req.params;
        const lines = req.body;

        if(!Array.isArray(lines) || lines.length === 0){
            res.sendStatus(400);
            return;
        }

        const invalidLines = validateLines(lines);

        if(invalidLines.length === 0 ){
            ticketService.amendTicket(ticketId, lines)
                .then(result => {
                    if(result === false){
                        res.status(400).send({
                            message: 'Ticket is locked, cannot add additional lines'
                        });
                        return;
                    }

                    res.send(result);
                    return;
                })
        }



    };



    return {
        createTicket,
        getAllTickets,
        getSingleTicket,
        amendTicketLines,
        getTicketStatus
    }
}

module.exports = setup;