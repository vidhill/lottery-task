/**
 * Created by davidhill on 11/11/2018.
 */

// generic wrapper for logger to allow easy replacement
const logger = require('signale');

module.exports = logger;
