const convict = require('convict');

const config = convict({
    db: {
        cbHost: {
            doc: "The couchbase connection string",
            format: String,
            default: 'localhost',
            env: "DB_HOST"
        },
        bucketId: {
            doc: "The couchbase db bucket",
            format: String,
            default: 'default',
            env: "DB_BUCKET"
        }
    },
    numberRange: {
        minNumber: {
            doc: "The highest lotto number",
            format: Number,
            default: 0,
            env: "NUMBER_MIN"
        },
        maxNumber: {
            doc: "The highest lotto number",
            format: Number,
            default: 2,
            env: "NUMBER_MAX"
        },
        count: {
            doc: "The number of numbers in a line",
            format: Number,
            default: 3,
            env: "NUMBER_COUNT"
        }
    }
});

config.validate({
    allowed: 'strict'
});

module.exports = config;